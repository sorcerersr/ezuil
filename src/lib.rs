//! # Introduction
//!
//! Ezuil = **E**a**z**y (easy) **u**ser **i**nterface **l**ayout
//!
//! Ezuil is a library for layouting user interfaces of games.
//! It is not meant to be a full GUI Toolkit, or a full featured LayoutManager,
//! but it provides the basics to easily place a element e.g on the top right
//! corner of the screen, by defining anchor points for both the screen and
//! the elements and then calculates the offsets.
//! Ezuil itself does not render anything it just provides the parameters that
//! can be used for rendering.
//!
//!

mod element;

pub use crate::element::{Element, ElementType};

pub type Point2 = nalgebra::Point2<f32>;

/// The `UserInterface` is the entry point for initializing the UI,
/// adding `Element`s and performing the layout. But it does not render anything
///  on the screen.
///
#[derive(Debug)]
pub struct UserInterface {
    // the width of the viewport
    actual_resolution: Point2,
    // the height of the viewport
    target_resolution: Point2,
    // the scaling factor that should globally applied to all elements
    global_scaling_factor: f32,
    // resolution based scaling factor
    resolution_scaling_factor: f32,
}

impl UserInterface {
    /// Creates a 'UserInterface' with a given target resolution and an
    /// actual resolution. With this two resolutions it is possible to define
    /// the sizes of UI elements for the target resolution (e.g. 1920x1080) and
    /// when a different resolution is used to scale the sizes appropriatly.
    pub fn new(target_resolution: Point2, actual_resolution: Point2) -> Self {
        // calculate the resolution based scaling factor
        // to keep the aspect ratio of elements it is only one factor
        // to be applied to both x and y scaling
        // Only the width is used to calculate the scaling factor
        // which might be not very accurate but for now it works
        let resolution_scaling_factor = actual_resolution.x / target_resolution.x;

        UserInterface {
            actual_resolution,
            target_resolution,
            global_scaling_factor: 1.0,
            resolution_scaling_factor,
        }
    }

    /// Create a frame element
    pub fn create_frame(&mut self, position: Anchor, width: f32, height: f32) -> Element {
        // the real coords for the view anchor, needed for further calculations
        let root = get_coords_for_anchor(
            &position,
            Point2::new(0.0, 0.0),
            self.actual_resolution,
            Point2::new(0.0, 0.0),
        );
        // scale the dimensions. the supplied width and height are for
        // the target resolution
        let scaled_dimensions = Point2::new(
            width * self.combined_scalingfactor(),
            height * self.combined_scalingfactor(),
        );
        // finally construct the frame
        Element::new_frame(position, root, scaled_dimensions)
    }

    /// Create a circle element
    pub fn create_circle(&mut self, position: Anchor, radius: f32) -> Element {
        // the real coords for the view anchor, needed for further calculations
        let root = get_coords_for_anchor(
            &position,
            Point2::new(0.0, 0.0),
            self.actual_resolution,
            Point2::new(0.0, 0.0),
        );
        // scale the radius
        let scaled_radius = radius* self.combined_scalingfactor();
        // finally construct the frame
        Element::new_circle(position, root, scaled_radius)
    }

    // get the combined scaling factor
    fn combined_scalingfactor(&self) -> f32 {
        self.global_scaling_factor * self.resolution_scaling_factor
    }
}

/// Determine and return the real screen coordinates for a given anchor.
fn get_coords_for_anchor(
    anchor: &Anchor,
    topleft: Point2,
    dimension: Point2,
    offset: Point2,
) -> Point2 {
    let root = Point2::new(topleft.x + offset.x, topleft.y + offset.y);

    match anchor {
        Anchor::TopLeft => Point2::new(root.x, root.y),
        Anchor::TopCenter => Point2::new(root.x + dimension.x / 2.0, root.y),
        Anchor::TopRight => Point2::new(root.x + dimension.x, root.y),
        Anchor::CenterLeft => Point2::new(root.x, root.y + dimension.y / 2.0),
        Anchor::CenterCenter => Point2::new(root.x + dimension.x / 2.0, root.y + dimension.y / 2.0),
        Anchor::CenterRight => Point2::new(root.x + dimension.x, root.y + dimension.y / 2.0),
        Anchor::BottomLeft => Point2::new(root.x, root.y + dimension.y),
        Anchor::BottomCenter => Point2::new(root.x + dimension.x / 2.0, root.y + dimension.y),
        Anchor::BottomRight => Point2::new(root.x + dimension.x, root.y + dimension.y),
    }
}

// get the offset for a given elements anchor
fn get_offset_for_anchor(dimension: Point2, anchor: &Anchor) -> Point2 {
    match anchor {
        Anchor::TopLeft => Point2::new(0.0, 0.0),
        Anchor::TopCenter => Point2::new(dimension.x / -2.0, 0.0),
        Anchor::TopRight => Point2::new(dimension.x * -1.0, 0.0),
        Anchor::CenterLeft => Point2::new(0.0, dimension.y / -2.0),
        Anchor::CenterCenter => Point2::new(dimension.x / -2.0, dimension.y / -2.0),
        Anchor::CenterRight => Point2::new(dimension.x * -1.0, dimension.y / -2.0),
        Anchor::BottomLeft => Point2::new(0.0, dimension.y / -1.0),
        Anchor::BottomCenter => Point2::new(dimension.x / -2.0, dimension.y / -1.0),
        Anchor::BottomRight => Point2::new(dimension.x / -1.0, dimension.y / -1.0),
    }
}

/// Enum defining the available anchor positions.
///
/// These anchors are used to describe both a position on the screen and a
/// anchor point on the element. Both together describes the position of the
/// element on the screen.
///
/// # Example
///
/// Screen Anchor: CenterCenter
///
/// Element Anchor: TopLeft
///
/// ```
///     TopLeft                    TopCenter                    TopRight
///     o-----------------------------o-----------------------------o
///     |                                                           |
///     |                                                           |
///     |                                                           |
///     |                                                           |
///     |                                                           |
///     |                                                           |
///     |                                                           |
///     | LeftCenter             CenterCenter           RightCenter |
///     o                              o----o----o                  o
///     |  This is a element, with its |         |                  |
///     |  TopLeft corner placed on the|    o    |                  |
///     |  CenterCenter Anchor of the  |         |                  |
///     |  screen                      o----o----o                  |
///     |                                                           |
///     |                                                           |
///     |                                                           |
///     | BottomLeft              BottomCenter          BottomRight |
///     o-----------------------------o-----------------------------o
/// ```
///
#[derive(Debug)]
pub enum Anchor {
    TopLeft,
    TopCenter,
    TopRight,
    CenterLeft,
    CenterCenter,
    CenterRight,
    BottomLeft,
    BottomCenter,
    BottomRight,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
