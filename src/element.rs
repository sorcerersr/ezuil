use crate::{Anchor, Point2};


/// Available primitive UI element types
#[derive(Debug)]
pub enum ElementType {
    Frame(Point2), // width and height
    Circle(f32),   // radius
}

#[derive(Debug)]
struct Positioning {
    // the real position on screen x,y coords without offset just both anchors
    // are already considered in calculation of the position
    position: Point2,
    // the offset to apply on top of the anchoring
    offset: Point2,
    // the anchor of the position (inside the parent)
    pos_anchor: Anchor,
    // own anchoring point
    own_anchor: Anchor,
}

/// This is a element of the user interface.
#[derive(Debug)]
pub struct Element {
    // position related data
    positioning: Positioning,
    // the type of the Element
    element_type: ElementType,
    // childs
    childs: Vec<Element>,
}

impl Element {
    /// Create a new frame element.
    pub fn new_frame(pos_anchor: Anchor, position: Point2, dimension: Point2) -> Self {
        let positioning = Positioning {
            position,
            offset: Point2::new(0.0, 0.0),
            pos_anchor,
            own_anchor: Anchor::TopLeft,
        };

        Element {
            positioning,
            element_type: ElementType::Frame(dimension),
            childs: Vec::new(),
        }
    }

    // create a new circle element.
    pub fn new_circle(pos_anchor: Anchor, position: Point2, radius: f32) -> Self {
        let positioning = Positioning {
            position,
            offset: Point2::new(0.0, 0.0),
            pos_anchor,
            own_anchor: Anchor::TopLeft,
        };

        Element {
            positioning,
            element_type: ElementType::Circle(radius),
            childs: Vec::new(),
        }
    }

    /// Apply a new anchor (using builder pattern)
    pub fn anchor(mut self, anchor: Anchor) -> Self {
        self.positioning.own_anchor = anchor;
        // recalculate offset based on new anchor
        let anchor_offset =
            crate::get_offset_for_anchor(self.get_dimension(), &self.positioning.own_anchor);

        crate::get_offset_for_anchor(self.get_dimension(), &self.positioning.own_anchor);

        // combine custom offset with anchor offest
        self.positioning.offset = Point2::new(
            self.positioning.offset.x + anchor_offset.x,
            self.positioning.offset.y + anchor_offset.y,
        );

        self
    }

    /// Apply a custom offset (using builder pattern)
    pub fn offset(mut self, offset: Point2) -> Self {
        // the offset needs to be combined with the anchor offset
        // get the anchor offset first
        let anchor_offset =
            crate::get_offset_for_anchor(self.get_dimension(), &self.positioning.own_anchor);

        // combine the given offset with the anchor offset
        self.positioning.offset = Point2::new(offset.x + anchor_offset.x, offset.y + anchor_offset.y);

        self
    }

    /// Get the ElementType
    pub fn get_type(&self) -> &ElementType {
        &self.element_type
    }

    /// Returns the dimensions of the element.
    ///
    /// For a frame this is width and height and for a circle the width
    /// and the height of a immaginary sorrounding box (so width and height is
    /// 2*radius for both values)
    fn get_dimension(&self) -> Point2 {
        match &self.element_type {
            ElementType::Frame(dimension) => *dimension,
            ElementType::Circle(radius) => Point2::new(radius * 2.0, radius * 2.0),
        }
    }

    /// Get the position of the element
    pub fn get_position(&self) -> Point2 {
        self.positioning.position
    }

    /// Get the offset to be applied to the elements position for correct
    /// positioning.
    pub fn get_offset(&self) -> Point2 {
        match &self.element_type {
            ElementType::Frame(_dimension) => self.positioning.offset,
            ElementType::Circle(radius) => {
                Point2::new(self.positioning.offset.x + radius, self.positioning.offset.y + radius)
            }
        }
    }

    /// Get a list of all childs as a reference
    /// (not meant to be modified, use add_child method to add new childs)
    pub fn get_childs(&self) -> &Vec<Element> {
        &self.childs
    }

    /// Adds a new child element.
    pub fn add_child(&mut self, mut child: Element) {
        // a element created by the create_frame method is by default a
        // top-level frame when adding it as a child its position must be
        // updated in relation to its parent which is no longer the screen but
        // another element
        let root = crate::get_coords_for_anchor(
            &child.positioning.pos_anchor,
            self.positioning.position,
            self.get_dimension(),
            self.positioning.offset,
        );

        child.positioning.position = root;

        self.childs.push(child);
    }
}
