//! This examples shows how to position UI elements using anchors.
//!
//! work in progress...
//!
extern crate ezuil;
use ggez::graphics::{self, Drawable};
use ggez::{conf, event, Context, GameResult};
use nalgebra;

struct State {
    ui_elements: Vec<ezuil::Element>,
}

use ezuil::Point2;

impl State {
    fn new(_ctx: &mut Context, width: f32, height: f32) -> Self {
        let target_resolution = Point2::new(1920.0, 1080.0);
        let current_resolution = Point2::new(width, height);

        let mut ui = ezuil::UserInterface::new(target_resolution, current_resolution);

        let mut rect1 = ui
            .create_frame(ezuil::Anchor::TopLeft, 300.0, 200.0)
            .anchor(ezuil::Anchor::TopLeft)
            .offset(Point2::new(10.0, 10.0));

        let child_frame1 = ui
            .create_frame(ezuil::Anchor::TopRight, 50.0, 50.0)
            .anchor(ezuil::Anchor::TopRight)
            .offset(Point2::new(-10.0, 10.0));

        let child_frame2 = ui
            .create_circle(ezuil::Anchor::BottomLeft, 25.0)
            .anchor(ezuil::Anchor::BottomLeft)
            .offset(Point2::new(10.0, -10.0));

        rect1.add_child(child_frame1);
        rect1.add_child(child_frame2);

        let rect2 = ui
            .create_frame(ezuil::Anchor::CenterCenter, 150.0, 150.0)
            .anchor(ezuil::Anchor::TopLeft);    

        let rect3 = ui
            .create_frame(ezuil::Anchor::CenterCenter, 150.0, 150.0)
            .anchor(ezuil::Anchor::BottomRight);    


        let circle = ui
            .create_circle(ezuil::Anchor::CenterCenter, 50.0)
            .anchor(ezuil::Anchor::CenterCenter);

        let ui_elements = vec![rect1, rect2, rect3, circle];

        State {
            ui_elements,
        }
    }
}

impl event::EventHandler for State {
    fn update(&mut self, _ctx: &mut Context) -> GameResult {
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, graphics::BLACK);

        for element in &self.ui_elements {
            draw_ui_element(ctx, &element)?;
        }

        graphics::present(ctx)?;
        Ok(())
    }
}

fn draw_ui_element(ctx: &mut Context, element: &ezuil::Element) -> GameResult {
    let ele_type = element.get_type();
    let position = element.get_position();
    let offset = element.get_offset();

    let color = if element.get_childs().is_empty() {
        graphics::Color::new(0.0, 0.0, 0.7, 1.0)
    } else {
        graphics::WHITE
    };

    match ele_type {
        ezuil::ElementType::Frame(dimensions) => {
            graphics::Mesh::new_rectangle(
                ctx,
                graphics::DrawMode::Fill,
                graphics::Rect::new(position.x, position.y, dimensions.x, dimensions.y),
                color,
            )
            .unwrap()
            .draw(ctx, (offset,))?;

            for child in element.get_childs() {
                draw_ui_element(ctx, child)?;
            }
        }
        ezuil::ElementType::Circle(radius) => {
            graphics::Mesh::new_circle(
                ctx,
                graphics::DrawMode::Fill,
                position,
                *radius,
                0.01,
                graphics::Color::new(0.7, 0.0, 0.0, 1.0),
            )
            .unwrap()
            .draw(ctx, (offset,))?;

            for child in element.get_childs() {
                draw_ui_element(ctx, child)?;
            }
        }
    }
    Ok(())
}

pub fn main() -> GameResult {
    let cb = ggez::ContextBuilder::new("anchor example", "ezuil")
        .window_setup(conf::WindowSetup::default().title("Anchor Example"))
        .window_mode(conf::WindowMode::default().dimensions(1024.0, 768.0));
    let (ctx, event_loop) = &mut cb.build()?;

    let state = &mut State::new(ctx, ctx.conf.window_mode.width, ctx.conf.window_mode.height);
    event::run(ctx, event_loop, state)
}
