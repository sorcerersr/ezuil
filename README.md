# Introduction

Ezuil = **E**a**z**y (easy) **u**ser **i**nterface **l**ayout

Ezuil is a library for layouting user interfaces of games.
It is not meant to be a full GUI Toolkit, or a full featured LayoutManager,
but it provides the basics to easily place a element e.g on the top right
corner of the screen, by defining anchor points for both the screen and
the elements and then calculates the offsets.
Ezuil itself does not render anything it just provides the parameters that
can be used for rendering.

# State

It is *work in progress* and very experimental.

# Examples

The *anchor* example is using ggez to show how positioning with anchors 
work. To run the example checkout the code and execute 

``` 
cargo run --example anchor 
```



